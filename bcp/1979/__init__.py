import json
import os

import nwgh.html

morning = {
    'collects': [
        'O God, you make us glad with the weekly remembrance of '
        'the glorious resurrection of your Son our Lord: Give us this '
        'day such blessing through our worship of you, that the week '
        'to come may be spent in your favour; through Jesus Christ our '
        'Lord.',

        'Almighty God, whose most dear Son went not up to joy but '
        'first he suffered pain, and entered not into glory before he '
        'was crucified: Mercifully grant that we, walking in the way '
        'of the cross, may find it none other than the way of life and '
        'peace; through Jesus Christ our Lord.',

        'Almighty God, who after the creation of the world rested '
        'from all your works and sanctified a day of rest for all your '
        'creatures: Grant that we, putting away all earthly anxieties, '
        'may be duly prepared for the service of your sanctuary, and '
        'that our rest here upon earth may be a preparation for the '
        'eternal rest promised to your people in heaven; through Jesus '
        'Christ our Lord.',

        'O God, the King eternal, whose light divides the day from the '
        'night and turns the shadow of death into the morning: Drive '
        'far from us all wrong desires, incline our hearts to keep your '
        'law, and guide our feet into the way of peace; that, having '
        'done your will with cheerfulness while it was day, we may, '
        'when night comes, rejoice to give you thanks; through Jesus '
        'Christ our Lord.',

        'O God, the author of peace and lover of concord, to know '
        'you is eternal life and to serve you is perfect freedom: Defend '
        'us, your humble servants, in all assaults of our enemies; that '
        'we, surely trusting in your defence, may not fear the power of '
        'any adversaries; through the might of Jesus Christ our Lord.',

        'Lord God, almighty and everlasting Father, you have '
        'brought us in safety to this new day: Preserve us with your '
        'mighty power, that we may not fall into sin, nor be overcome '
        'by adversity; and in all we do, direct us to the fulfilling of '
        'your purpose; through Jesus Christ our Lord.',

        'Heavenly Father, in you we live and move and have our '
        'being: We humbly pray you so to guide and govern us by '
        'your Holy Spirit, that in all the cares and occupations of our '
        'life we may not forget you, but may remember that we are '
        'ever walking in your sight; through Jesus Christ our Lord.'
    ],

    'prayers_for_mission': [
        'Almighty and everlasting God, by whose Spirit the whole '
        'body of your faithful people is governed and sanctified: '
        'Receive our supplications and prayers which we offer before '
        'you for all members of your holy Church, that in their '
        'vocation and ministry they may truly and devoutly serve you; '
        'through our Lord and Saviour Jesus Christ.',

        'O God, you have made of one blood all the peoples of the '
        'earth, and sent your blessed Son to preach peace to those '
        'who are far off and to those who are near: Grant that people '
        'everywhere may seek after you and find you; bring the '
        'nations into your fold; pour out your Spirit upon all flesh; '
        'and hasten the coming of your kingdom; through Jesus '
        'Christ our Lord.',

        'Lord Jesus Christ, you stretched out your arms of love on '
        'the hard wood of the cross that everyone might come within '
        'the reach of your saving embrace: So clothe us in your Spirit '
        'that we, reaching forth our hands in love, may bring those '
        'who do not know you to the knowledge and love of you; for '
        'the honour of your Name.'
    ]
}

noonday = {
    'collects': [
        'Heavenly Father, send your Holy Spirit into our hearts, to '
        'direct and rule us according to your will, to comfort us in all '
        'our afflictions, to defend us from all error, and to lead us '
        'into all truth; through Jesus Christ our Lord.',

        'Blessed Saviour, at this hour you hung upon the cross, '
        'stretching out your loving arms: Grant that all the peoples of '
        'the earth may look to you and be saved; for your tender '
        "mercies' sake.",

        'Almighty Saviour, who at noonday called your servant Saint Paul '
        'to be an apostle to the Gentiles: We pray you to illumine the '
        'world with the radiance of your glory, that all nations may '
        'come and worship you; for you live and reign for ever and ever.',

        'Lord Jesus Christ, you said to your apostles, "Peace I give to '
        'you; my peace I leave with you:" Regard not our sins, but the '
        'faith of your Church, and give to us the peace and unity of '
        'that heavenly city, where with the Father and the Holy Spirit '
        'you live and reign, now and for ever.'
    ],
    'psalms': [
        {
            'passage': '119:105-112',
            'text': [
                'Your word is a lantern to my feet, and a light upon my '
                'path.',

                'I have sworn and am determined to keep your righteous '
                'judgements.',

                'I am deeply troubled; preserve my life, O <span '
                'class="lord">Lord</span>, according to your word.',

                'Accept, O <span class="lord">Lord</span>, the willing '
                'tribute of my lips, and teach me your judgements.',

                'My life is always in my hand, yet I do not forget your '
                'law.',

                'The wicked have set a trap for me, but I have not '
                'strayed from your commandments.',

                'Your decrees are my inheritance for ever; truly, they '
                'are the joy of my heart.',

                'I have applied my heart to fulfil your statutes for '
                'ever and to the end.'
            ]
        },
        {
           'passage': '121:1-8',
           'text': [
               'I lift up my eyes to the hills; from where is my help '
               'to come?',

               'My help comes from the <span class="lord">Lord</span>, '
               'the maker of heaven and earth.',

               'He will not let your foot be moved and he who watches '
               'over you will not fall asleep.',

               'Behold, he who keeps watch over Israel shall neither '
               'slumber nor sleep;',

               'The <span class="lord">Lord</span> himself watches over '
               'you; the <span class="lord">Lord</span> is your shade '
               'at your right hand,',

               'So that the sun shall not strike you by day, nor the '
               'moon by night.',

               'The <span class="lord">Lord</span> shall preserve you '
               'from all evil; it is he who shall keep you safe.',

               'The <span class="lord">Lord</span> shall watch over '
               'your going out and your coming in, from this time forth '
               'for evermore.'
            ]
       },
       {
           'passage': '126:1-7',
           'text': [
               'When the <span class="lord">Lord</span> restored the '
               'fortunes of Zion, then we were like those who dream.',

               'Then our mouth was filled with laughter, and our tongue '
               'with shouts of joy.',

               'They said among the nations, "The <span '
               'class="lord">Lord</span> has done great things for '
               'them."',

               'The <span class="lord">Lord</span> has done great '
               'things for us, and we are glad indeed.',

               'Restore our fortunes, O <span class="lord">Lord</span>, '
               'like the watercourses of the Negev.',

               'Those who sowed with tears will reap with songs of joy.',

               'Those who go out weeping, carrying the seed, will come '
               'again with joy, shouldering their sheaves.'
           ]
       }
    ],
    'scripture': [
        {
            'passage': 'Romans 5:5',
            'text': 'The love of God has been poured into our hearts through the '
                    'Holy Spirit that has been given to us.'
        },
        {
            'passage': '2 Corinthians 5:17-18',
            'text': 'If anyone is in Christ he is a new creation; the old has passed '
                    'away, behold the new has come. All this is from God, who '
                    'through Christ reconciled us to himself and gave us the ministry '
                    'of reconciliation.'
        },
        {
            'passage': 'Malachi 1:11',
            'text': 'From the rising of the sun to its setting my Name shall be '
                    'great among the nations, and in every place incense shall be '
                    'offered to my Name, and a pure offering; for my Name shall be '
                    'great among the nations, says the Lord of Hosts.'
        }
    ]
}

evening = {
    'collects': [
        'Lord God, whose Son our Saviour Jesus Christ triumphed over '
        'the powers of death and prepared for us our place in the new '
        'Jerusalem: Grant that we, who have this day given thanks for '
        'his resurrection, may praise you in that City of which he is the '
        'light, and where he lives and reigns for ever and ever.',

        'Lord Jesus Christ, by your death you took away the sting of '
        'death: Grant to us your servants so to follow in faith where '
        'you have led the way, that we may at length fall asleep '
        'peacefully in you and wake up in your likeness; for your '
        'tender mercies\' sake.',

        'O God, the source of eternal light: Shed forth your unending '
        'day upon us who watch for you, that our lips may praise you, '
        'our lives may bless you, and our worship on the morrow give '
        'you glory; through Jesus Christ our Lord.',

        'Most holy God, the source of all good desires, all right '
        'judgements, and all just works: Give to us, your servants, that '
        'peace which the world cannot give, so that our minds may be '
        'fixed on the doing of your will, and that we, being delivered '
        'from the fear of all enemies, may live in peace and quietness; '
        'through the mercies of Christ Jesus our Saviour.',

        'Be our light in the darkness, O Lord, and in your great mercy '
        'defend us from all perils and dangers of this night; for the love '
        'of your only Son, our Saviour Jesus Christ.',

        'O God, the life of all who live, the light of the faithful, the '
        'strength of those who labour, and the repose of the dead: We '
        'thank you for the blessings of the day that is past, and '
        'humbly ask for your protection through the coming night. '
        'Bring us in safety to the morning hours; through him who '
        'died and rose again for us, your Son our Saviour Jesus Christ.',

        'Lord Jesus, stay with us, for evening is at hand and the day '
        'is past; be our companion in the way, kindle our hearts, and '
        'awaken hope, that we may know you as you are revealed in '
        'Scripture and the breaking of bread. Grant this for the sake '
        'of your love.'
    ],

    'prayers_for_mission': [
        'O God and Father of all, whom the whole heavens adore: '
        'Let the whole earth also worship you, all nations obey you, '
        'all tongues confess and bless you, and men and women '
        'everywhere love you and serve you in peace; through Jesus '
        'Christ our Lord.',

        'Keep watch, dear Lord, with those who work, or watch, or '
        'weep this night, and give your angels charge over those who '
        'sleep. Tend the sick, Lord Christ; give rest to the weary, bless '
        'the dying, soothe the suffering, pity the afflicted, shield the '
        'joyous; and all for your love\'s sake.',

        'O God, you manifest in your servants the signs of your '
        'presence: Send forth upon us the spirit of love, that in '
        'companionship with one another your abounding grace may '
        'increase among us; through Jesus Christ our Lord.'
    ]
}

compline = {
    'collects': [
        'Be our light in the darkness, O Lord, and in your great mercy '
        'defend us from all perils and dangers of this night; for the '
        'love of your only Son, our Saviour Jesus Christ.',

        'Be present, O merciful God, and protect us through the hours of '
        'this night, so that we who are wearied by the changes and '
        'chances of this life may rest in your eternal changelessness; '
        'through Jesus Christ our Lord.',

        'Look down, O Lord, from your heavenly throne, and illumine this '
        'night with your celestial brightness; that by night as by day '
        'your people may glorify your holy Name; through Jesus Christ '
        'our Lord.',

        'Visit this place, O Lord, and drive far from it all snares of '
        'the enemy; let your holy angels dwell with us to preserve us in '
        'peace; and let your blessing be upon us always; through Jesus '
        'Christ our Lord.',

        # Saturday only
        'We give you thanks, O God, for revealing '
        'your Son Jesus Christ to us by the light of '
        'his resurrection: Grant that as we sing your '
        'glory at the close of this day, our joy may '
        'abound in the morning as we celebrate the '
        'Paschal mystery; through Jesus Christ our '
        'Lord.'
    ],
    'psalms': [
        {
            'passage': '4:1-8',
            'text': [
                'Answer me when I call, O God, defender of my cause; you '
                'set me free when I am hard-pressed; have mercy on me '
                'and hear my prayer.',

                '"You mortals, how long will you dishonour my glory; how '
                'long will you worship dumb idols and run after false '
                'gods?"',

                'Know that the <span class="lord">Lord</span> does '
                'wonders for the faithful; when I call upon the <span '
                'class="lord">Lord</span>, he will hear me.',

                'Tremble, then, and do not sin; speak to your heart in '
                'silence upon your bed.',

                'Offer the appointed sacrifices and put your trust in '
                'the <span class="lord">Lord</span>.',

                'Many are saying, "Oh, that we might see better times!" '
                'Lift up the light of your countenance upon us, O <span '
                'class="lord">Lord</span>.',

                'You have put gladness in my heart, more than when grain '
                'and wine and oil increase.',

                'I lie down in peace; at once I fall asleep; for only '
                'you, <span class="lord">Lord</span>, make me dwell in '
                'safety.'
            ]
        },
        {
            'passage': '31:1-5',
            'text': [
                'In you, O <span class="lord">Lord</span>, have I taken '
                'refuge; let me never be put to shame: deliver me in '
                'your righteousness.',

                'Incline your ear to me; make haste to deliver me.',

                'Be my strong rock, a castle to keep me safe, for you '
                'are my crag and my stronghold; for the sake of your '
                'Name, lead me and guide me.',

                'Take me out of the net that they have secretly set for '
                'me, for you are my tower of strength.',

                'Into your hands I commend my spirit, for you have '
                'redeemed me, O <span class="lord">Lord</span>, O God of '
                'truth.'
            ]
        },
        {
            'passage': '91:1-16',
            'text': [
                'He who dwells in the shelter of the Most High abides '
                'under the shadow of the Almighty.',

                'He shall say to the <span class="lord">Lord</span>, '
                '"You are my refuge and my stronghold, my God in whom I '
                'put my trust."',

                'He shall deliver you from the snare of the hunter and '
                'from the deadly pestilence.',

                'He shall cover you with his pinions, and you shall find '
                'refuge under his wings; his faithfulness shall be a '
                'shield and buckler.',

                'You shall not be afraid of any terror by night, nor of '
                'the arrow that flies by day;',

                'Of the plague that stalks in the darkness, nor of the '
                'sickness that lays waste at mid-day.',

                'A thousand shall fall at your side and ten thousand at '
                'your right hand, but it shall not come near you.',

                'Your eyes only have to behold to see the reward of the '
                'wicked.',

                'Because you have made the <span '
                'class="lord">Lord</span> your refuge, and the Most High '
                'your habitation,',

                'There shall no evil happen to you, neither shall any '
                'plague come near your dwelling.',

                'For he shall give his angels charge over you, to keep '
                'you in all your ways.',

                'They shall bear you in their hands, lest you dash your '
                'foot against a stone.',

                'You shall tread upon the lion and the adder; you shall '
                'trample the young lion and the serpent under your feet.',

                'Because he is bound to me in love, therefore will I '
                'deliver him; I will protect him, because he knows my '
                'Name.',

                'He shall call upon me, and I will answer him; I am with '
                'him in trouble; I will rescue him and bring him to '
                'honour.',

                'With long life will I satisfy him, and show him my '
                'salvation.'
            ]
        },
        {
            'passage': '134:1-2',
            'text': [
                'Behold now, bless the <span class="lord">Lord</span>, '
                'all you servants of the <span class="lord">Lord</span>, '
                'you that stand by night in the house of the <span '
                'class="lord">Lord</span>.',

                'Lift up your hands in the holy place and bless the '
                '<span class="lord">Lord</span>; the <span '
                'class="lord">Lord</span> who made heaven and earth '
                'bless you out of Zion.'
            ]
        }
    ],

    'scripture': [
        {
            'passage': 'Jeremiah 14:9,22',
            'text': 'Lord, you are in the midst of us, and we are called by your '
                    'Name: Do not forsake us, O Lord our God.'
        },
        {
            'passage': 'Matthew 11:28-30',
            'text': 'Come to me, all who labour and are heavy-laden, and I will '
                    'give you rest. Take my yoke upon you, and learn from me; '
                    'for I am gentle and lowly in heart, and you will find rest for '
                    'your souls. For my yoke is easy, and my burden is light.'
        },
        {
            'passage': 'Hebrews 13:20-21',
            'text': 'May the God of peace, who brought again from the dead our '
                    'Lord Jesus, the great shepherd of the sheep, by the blood of '
                    'the eternal covenant, equip you with everything good that you '
                    'may do his will, working in you that which is pleasing in his '
                    'sight; through Jesus Christ, to whom be glory for ever and '
                    'ever.'
        },
        {
            'passage': '1 Peter 5:8-9a',
            'text': 'Be sober, be watchful. Your adversary the devil prowls '
                    'around like a roaring lion, seeking someone to devour. '
                    'Resist him, firm in your faith.'
        }
    ]
}

lectionary = json.load(open(os.path.join(os.path.dirname(__file__), 'lectionary.json')))
collects = json.load(open(os.path.join(os.path.dirname(__file__), 'collects.json')))


def render_mp(config):
    if config['day'] == 'sunday':
        fixed_collect_idx = 0
        prayer_for_mission_idx = 1
    elif config['day'] == 'monday':
        fixed_collect_idx = 3
        prayer_for_mission_idx = 0
    elif config['day'] == 'tuesday':
        fixed_collect_idx = 4
        prayer_for_mission_idx = 1
    elif config['day'] == 'wednesday':
        fixed_collect_idx = 5
        prayer_for_mission_idx = 2
    elif config['day'] == 'thursday':
        fixed_collect_idx = 6
        prayer_for_mission_idx = 0
    elif config['day'] == 'friday':
        fixed_collect_idx = 1
        prayer_for_mission_idx = 1
    elif config['day'] == 'saturday':
        fixed_collect_idx = 2
        prayer_for_mission_idx = 2

    if 'collect_day' in config:
        daily_collect = collects[config['collect_day']]
    else:
        daily_collect = collects[config['week']]
    fixed_collect = morning['collects'][fixed_collect_idx]
    prayer_for_mission = morning['prayers_for_mission'][prayer_for_mission_idx]

    return nwgh.html.render_template('templates/mp.html')


def render_np(config):
    if config['day'] == 'sunday':
        collect_idx = 2
        scripture_idx = 1
    elif config['day'] == 'monday':
        collect_idx = 0
        scripture_idx = 0
    elif config['day'] == 'tuesday':
        collect_idx = 1
        scripture_idx = 1
    elif config['day'] == 'wednesday':
        collect_idx = 2
        scripture_idx = 2
    elif config['day'] == 'thursday':
        collect_idx = 3
        scripture_idx = 0
    elif config['day'] == 'friday':
        collect_idx = 0
        scripture_idx = 1
    elif config['day'] == 'saturday':
        collect_idx = 1
        scripture_idx = 2

    collect = noonday['collects'][collect_idx]
    psalm = noonday['psalms'][scripture_idx]
    scripture = noonday['scripture'][scripture_idx]

    psalm_passage = f"Psalm {psalm['passage']}"
    psalm = config['format_passage'](psalm_passage, psalm['text'])

    return nwgh.html.render_template('templates/np.html')


def render_ep(config):
    if config['day'] == 'sunday':
        fixed_collect_idx = 0
        prayer_for_mission_idx = 1
    elif config['day'] == 'monday':
        fixed_collect_idx = 3
        prayer_for_mission_idx = 0
    elif config['day'] == 'tuesday':
        fixed_collect_idx = 4
        prayer_for_mission_idx = 1
    elif config['day'] == 'wednesday':
        fixed_collect_idx = 5
        prayer_for_mission_idx = 2
    elif config['day'] == 'thursday':
        fixed_collect_idx = 6
        prayer_for_mission_idx = 0
    elif config['day'] == 'friday':
        fixed_collect_idx = 1
        prayer_for_mission_idx = 1
    elif config['day'] == 'saturday':
        fixed_collect_idx = 2
        prayer_for_mission_idx = 2

    if 'collect_day' in config:
        daily_collect = collects[config['collect_day']]
    else:
        daily_collect = collects[config['week']]
    fixed_collect = evening['collects'][fixed_collect_idx]
    prayer_for_mission = evening['prayers_for_mission'][prayer_for_mission_idx]

    return nwgh.html.render_template('templates/ep.html')


def render_cp(config):
    if config['day'] == 'sunday':
        collect_idx = 1
        scripture_idx = 2
    elif config['day'] == 'monday':
        collect_idx = 0
        scripture_idx = 0
    elif config['day'] == 'tuesday':
        collect_idx = 1
        scripture_idx = 1
    elif config['day'] == 'wednesday':
        collect_idx = 2
        scripture_idx = 2
    elif config['day'] == 'thursday':
        collect_idx = 3
        scripture_idx = 3
    elif config['day'] == 'friday':
        collect_idx = 0
        scripture_idx = 0
    elif config['day'] == 'saturday':
        collect_idx = 4
        scripture_idx = 1

    collect = compline['collects'][collect_idx]
    psalm = compline['psalms'][scripture_idx]
    scripture = compline['scripture'][scripture_idx]

    psalm_passage = f"Psalm {psalm['passage']}"
    psalm = config['format_passage'](psalm_passage, psalm['text'])

    return nwgh.html.render_template('templates/cp.html')
