#!/usr/bin/env python3

import csv
import json
import re
import sys


def parse_psalms(psalmtext):
    return list(map(lambda x: x.strip(), psalmtext.split(',')))


def parse_lectionary(f):
    week = None
    days = []
    rval = {}

    reader = csv.DictReader(f)
    for row in reader:
        if row['Notes']:
            print('Notes: %s' % (row['Notes'],))

    return rval


year1 = {}
with open('daily_lectionary1.csv') as f:
    print('year1')
    year1 = parse_lectionary(f)

year2 = {}
with open('daily_lectionary2.csv') as f:
    print('year2')
    year2 = parse_lectionary(f)
