#!/usr/bin/env python3

import argparse
import bs4
import importlib
import inspect
import io
import jinja2
import os
import re
import requests
import sys
import traceback
import uuid
import zipfile

import nwgh.html
import nwgh.prog

DAYS = ('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday')


def get_verses(html_text):
    html = bs4.BeautifulSoup(html_text, features='lxml')
    text = html.find('div', attrs={'class': 'bibletext'})
    # Remove a bunch of things that are a mess
    to_extract = []
    for descendant in text.descendants:
        if not isinstance(descendant, (bs4.element.NavigableString, bs4.element.Tag)):
            # Non-comment misc tags
            to_extract.append(descendant)
        elif isinstance(descendant, bs4.element.Comment):
            to_extract.append(descendant)
    for e in to_extract:
        e.extract()
    for a in text.findAll('a'):
        a.extract()
    for h in text.findAll('h2'):
        h.extract()
    for b in text.findAll('br'):
        b.extract()
    for p in text.findAll('p'):
        # Remove unnecessary paragraph wrappings
        p.unwrap()

    verses = []
    verse = ''
    for child in text.children:
        if isinstance(child, bs4.element.NavigableString):
            s = str(child).strip('\n') # Get rid of useless newlines
            s = s.replace('\u2018', '&lsquo;').replace('\u2019', '&rsquo;') # Make unicode quotes ascii quotes
            s = s.replace('\x91', '&lsquo;').replace('\x92', '&rsquo;') # Make more unicode quotes ascii quotes
            s = s.replace('\x93', '&ldquo;').replace('\x94', '&rdquo;') # Make unicode double quotes ascii double quotes
            s = s.replace('\x97', '&mdash;') # Make unicode em-dash into ascii
            s = s.replace('\xa0', ' ') # Strip out line feeds
            if verse and s and not s.startswith(' ') and (s[0].isalnum() or s[0] in '\'"'):
                verse += ' ' # Ensure a space between words
            verse += s
        else: # It's a tag
            if 'vnumVis' in child.attrs.get('class', ''):
                # This marks the beginning of a new verse, so append the
                # previous one and start a new one.
                if verse:
                    # Sometimes we start with an empty string, so ignore those
                    # Take this opportunity to coalesce all whitespace in the verse into a single space.
                    verses.append(re.sub('\s+', ' ', verse))
                verse = ''
            elif 'sc' in child.attrs.get('class', ''):
                # This is a span that contains LORD. Important to keep this in, so
                # let's do so.
                verse += ' <span class="lord">Lord</span>'
    if verse:
        verses.append(verse)

    return verses


def retrieve_passage(passage):
    url = 'https://bible.oremus.org/'
    params = {
        'version': 'NRSVAE',
        'vnum': 'NO',
        'fnote': 'NO',
        'show_ref': 'NO',
        'headings': 'NO',
        'omit_hidden': 'YES',
        'passage': passage
    }
    res = requests.get(url, params)

    if not res.ok:
        raise Exception(f'Could not retrieve {passage}')

    return get_verses(res.text)


def get_verse_range(passage, nverses):
    verses = []
    if ',' in passage:
        # Disjoint ranges
        book, ranges = passage.split(':')
        ranges = ranges.split(',')
        for vrange in ranges:
            first, last = vrange.split('-')
            verses.extend(range(int(first.strip('ab')), int(last.strip('ab')) + 1))
    elif passage.count(':') == 2:
        # Crosses chapters
        bv1, bv2 = passage.split('-')
        book1, verse1 = bv1.split(':')
        book2, verse2 = bv2.split(':')
        b2verses = range(1, int(verse2.strip('ab')) + 1)
        b1count = nverses - len(b2verses)
        verse1 = int(verse1.strip('ab'))
        b1verses = range(verse1, verse1 + b1count)
        verses = list(b1verses) + list(b2verses)
    else:
        # Should be simple and straightforward if we get here
        book, verses = passage.split(':')
        first, last = verses.split('-')
        verses = list(range(int(first.strip('ab')), int(last.strip('ab')) + 1))

    return verses


def format_passage(passage, text):
    nverses = len(text)
    if '-' in passage:
        versenos = get_verse_range(passage, nverses)
    else:
        versenos = list(range(1, nverses + 1))

    verseno_len = len(versenos)
    assert verseno_len >= nverses, f'{passage} has too few verses ({verseno_len}) for verses ({nverses})'
    verses = [{'number': versenos[i], 'text': text[i]} for i in range(nverses)]
    return nwgh.html.render_template('templates/passage.html')


def normalize_passage(passage):
    book, cv = passage.strip().rsplit(' ', 1)
    if '-' not in cv:
        # No verse range, so what we had is the right stuff already
        return passage

    cv = cv.strip()
    span_chapter = re.compile('(\d+):([\dab]+)-(\d+):([\dab]+)')
    span = span_chapter.match(cv)
    if span:
        # These are generally pretty simple
        return f'{book} {cv}'

    if ',' in cv:
        # Also usually pretty simple
        return f'{book} {cv}'

    pattern = re.compile('(\d+):([\dab-]+)?(\([\dab-]+\))?([\dab-]+)?(.*)')
    match = pattern.match(cv.strip())
    chapter = match.group(1)
    verses = list(map(lambda x: x.strip().strip('()'), filter(None, (match.group(i) for i in range(2, 5)))))
    rest = match.group(5)

    if '-' in verses[0]:
        start, end = verses[0].split('-')
    else:
        start = end = verses[0]
    verses = verses[1:]
    for vrange in verses:
        if '-' in vrange:
            a, b = vrange.split('-')
        else:
            a = b = vrange
        if 'a' in end:
            if 'b' not in a:
                raise Exception(f"Can't handle disjoint partial ranges: {passage}")
            iend = int(end.strip('a'))
            ia = int(a.strip('b'))
            if ia == iend:
                end = b
            else:
                raise Exception(f"Can't handle optional disjoint ranges: {passage}")
        elif int(a) == int(end) + 1:
            end = b
        else:
            # TODO - handle disjoint ranges
            raise Exception(f"Can't handle optional disjoint ranges: {passage}")

    if rest:
        # TODO - handle passages that span chapters
        raise Exception(f"Can't handle optional passages that span chapters: {passage}")

    return f'{book} {chapter}:{start}-{end}'


def generate_morning_prayer(bcp, config):
    text = bcp.render_mp(config)
    return nwgh.html.render_template('templates/morning.html')


def generate_noonday_prayer(bcp, config):
    text = bcp.render_np(config)
    return nwgh.html.render_template('templates/noonday.html')


def generate_evening_prayer(bcp, config):
    text = bcp.render_ep(config)
    return nwgh.html.render_template('templates/evening.html')


def generate_compline(bcp, config):
    text = bcp.render_cp(config)
    return nwgh.html.render_template('templates/compline.html')


def format_psalms(psalms):
    formatted_psalms = []
    for psalm in psalms:
        psalm = normalize_passage(f'Psalm {psalm}')
        psalm_text = retrieve_passage(psalm)
        formatted_psalms.append(format_passage(psalm, psalm_text))
    return formatted_psalms


def generate_prayer(bcp, year, week, out_fmt):
    prayers = []
    config = {'year': year, 'week': week, 'format_passage': format_passage}
    for scripture, day in zip(bcp.lectionary[week], DAYS):
        config['day'] = day

        ot = normalize_passage(scripture[year]['ot'])
        ot_text = retrieve_passage(ot)
        config['ot'] = format_passage(ot, ot_text)

        nt = normalize_passage(scripture[year]['nt'])
        nt_text = retrieve_passage(nt)
        config['nt'] = format_passage(nt, nt_text)

        gospel = normalize_passage(scripture[year]['gospel'])
        gospel_text = retrieve_passage(gospel)
        config['gospel'] = format_passage(gospel, gospel_text)

        config['psalms'] = format_psalms(scripture['psalms']['morning'])
        prayers.append(generate_morning_prayer(bcp, config))
        del config['psalms']
        prayers.append(generate_noonday_prayer(bcp, config))
        config['psalms'] = format_psalms(scripture['psalms']['evening'])
        prayers.append(generate_evening_prayer(bcp, config))
        del config['psalms']
        prayers.append(generate_compline(bcp, config))

    with open('prayer.css') as f:
        css = f.read()

    if out_fmt == 'html':
        return nwgh.html.render_template('templates/web-prayer.html')

    return nwgh.html.render_template('templates/ebook-prayer.html')


def write_zipfile(year, week, start_xhtml, outfile):
    ebook_uuid = uuid.uuid4()
    calibre_uuid = uuid.uuid4()
    metadata_opf = nwgh.html.render_template('templates/ebook/metadata.opf')
    toc_ncx = nwgh.html.render_template('templates/ebook/toc.ncx')
    mimetype = open('templates/ebook/mimetype').read()
    META_INF__container_xml = open('templates/ebook/META-INF/container.xml').read()

    with zipfile.ZipFile(outfile, 'w', compression=zipfile.ZIP_DEFLATED) as z:
        z.writestr('mimetype', mimetype)
        z.writestr('metadata.opf', metadata_opf)
        z.writestr('start.xhtml', start_xhtml)
        z.writestr('toc.ncx', toc_ncx)
        z.writestr('META-INF/container.xml', META_INF__container_xml)


@nwgh.prog.main
def main(argv):
    bcp_choices = set(filter(lambda x: not x.startswith('__'), os.listdir('bcp')))
    assert('1979' in bcp_choices)

    parser = argparse.ArgumentParser()
    parser.add_argument('--week', required=True)
    parser.add_argument('--year', required=True)
    parser.add_argument('--bcp', default='1979', choices=bcp_choices)
    parser.add_argument('--output')
    parser.add_argument('--format', default='html', choices=('html', 'epub'))
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args(argv)

    if args.format == 'epub' and not args.output:
        nwgh.prog.die('epub output format requires an output file')

    bcp = importlib.import_module(f'bcp.{args.bcp}')

    try:
        prayer = generate_prayer(bcp, args.year, args.week, args.format)
    except:
        if args.debug:
            etype, evalue, etb = sys.exc_info()
            traceback.print_exception(etype, evalue, etb)
            sys.exit(1)
        else:
            raise

    if args.format == 'html':
        if args.output:
            outfile = open(args.output, 'w')
        else:
            outfile = sys.stdout

        outfile.write(prayer)
    else:
        write_zipfile(args.year, args.week, prayer, args.output)
